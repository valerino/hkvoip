#!/usr/bin/env bash
TO_DOWNLOAD=$1
VERSION=$2
DEST_BASE=$3

if [ $# != 3 ]; then
  echo usage: $0 \<arch[,arch,...]\> \<version\> \<dest_base\>
  echo one or more directories named \<devkit-arch\> will be created in \<dest_base\>
  echo
  echo ex. download version 10.7.7 for android to current folder: i.e. download_frida.sh android-arm,android-arm64 10.7.7 .
  echo check https://github.com/frida/frida/releases for latest release!
  exit 1
fi

declare -a archs=("linux-x86" "linux-x86_64" "ios-arm" "ios-arm64" "macos-x86_64" "android-arm" "android-arm64")
declare -a archs_srv=("linux-x86" "linux-x86_64" "ios-arm" "ios-arm64" "macos-x86_64" "android-arm" "android-arm64")
declare -a archs_gad=("linux-x86" "linux-x86_64" "ios-universal" "macos-universal" "android-arm" "android-arm64")

function _download_internal() {
# $1 is 'core', 'gum', 'server'
# $2 is os-arch
# $3 is dest base
# $4 is 1 for server
  echo Downloading\: $1-$2 to\: $3

  if [ $4 -eq 1 ]; then
    # server
    curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.xz -o /tmp/tmp.xz
  elif [ $4 -eq 2 ]; then
    # gadget
    if [ "$2" == 'macos-universal' ] || [ "$2" == 'ios-universal' ]; then
      curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.dylib.xz -o /tmp/tmp.xz
    else
      curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-$VERSION-$2.so.xz -o /tmp/tmp.xz
    fi
  else
    # gum / core
    curl -L https://github.com/frida/frida/releases/download/$VERSION/frida-$1-devkit-$VERSION-$2.tar.xz -o /tmp/tmp.tar.gz
  fi

  # create directories
  mkdir -p $3/devkit-$2/lib
  mkdir -p $3/devkit-$2/include
  mkdir -p $3/devkit-$2/bin
  if [ $? -eq 0 ]; then
    if [ $4 -eq 1 ]; then
      # server
      xz -d -c /tmp/tmp.xz > $3/devkit-$2/bin/frida-server
    elif [ $4 -eq 2 ]; then
      # gadget
      if [ "$2" == 'macos-universal' ] || [ "$2" == 'ios-universal' ]; then
        xz -d -c /tmp/tmp.xz > $3/devkit-$2/bin/frida-gadget.dylib
      else
        xz -d -c /tmp/tmp.xz > $3/devkit-$2/bin/frida-gadget.so
      fi
    else
      # gum / core
      mkdir -p /tmp/bla
      tar xvfJ /tmp/tmp.tar.gz -C /tmp/bla
      mv /tmp/bla/*.a $3/devkit-$2/lib
      mv /tmp/bla/*.h $3/devkit-$2/include
      rm -rf /tmp/bla
    fi
  fi
}

function _download_internal_pre() {
# $1 is 'core', 'gum'
# $2 is dest base
# $3 is 1 for server, 2 for gadget
# $4 is the desired arch (single)
  if [ $3 -eq 1 ]; then
    # server
    for arch in "${archs_srv[@]}"
      do
        if [ $4 = $arch ]; then 
          _download_internal $1 $arch $2 $3
        fi
      done
  elif [ $3 -eq 2 ]; then
    # gadget
    for arch in "${archs_gad[@]}"
    do
     if [ $4 = $arch ]; then 
      _download_internal $1 $arch $2 $3
     fi
    done
  else
    # gum/core
    for arch in "${archs[@]}"
    do
      if [ $4 = $arch ]; then 
        _download_internal $1 $arch $2 $3
      fi
    done
  fi
}

function download() {
# $1 is 'core', 'gum'
# $2 is dest base
# $3 is 1 for server, 2 for gadget
# $4 is the desired arch (may be , separated)
  __ARCHS=$4
  if [[ $__ARCHS = *","* ]]; then
    for i in ${__ARCHS//,/ }
    do
      _download_internal_pre $1 $2 $3 $i
    done
  else
    # single
    _download_internal_pre $1 $2 $3 $4
  fi
}

# download cores
echo 'Downloading FRIDA cores'
download core $DEST_BASE 0 $TO_DOWNLOAD

#
# download gums
echo 'Downloading FRIDA gums'
download gum $DEST_BASE 0 $TO_DOWNLOAD

#
# download server
echo 'Downloading FRIDA servers'
download server $DEST_BASE 1 $TO_DOWNLOAD

# download gadgets
# echo 'Downloading FRIDA gadgets'
# download gadget $DEST_BASE 2 $TO_DOWNLOAD

# done
rm /tmp/tmp.xz
rm /tmp/tmp.tar.gz

exit 0
