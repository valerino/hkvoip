#ifndef __fileapi_h__
#define __fileapi_h__

#ifdef __cplusplus
extern "C" {
#endif
/*
 * @brief get file size from file pointer
 *
 * @param f the file
 * @param size on successful return, file size
 * @return 0 on success
 */
int file_get_size_fp(FILE *f, size_t *size);

/*
 * @brief get file size
 *
 * @param path the file
 * @param size on successful return, file size
 * @return 0 on success
 */
int file_get_size(const char *path, size_t *size);

/**
 * @brief recursive delete a folder
 *
 * @param dir path to the directory to be deleted
 * @return 0 on success
 */
int file_recursive_delete(const char *dir);

/**
 * @brief read whole file to buffer
 *
 * @param path path to file to be read
 * @param buf on successful return, file buffer to be freed with free()
 * @param size on successful return, buffer size
 * @return 0 on success
 */
int file_to_buffer(const char *path, unsigned char **buf, size_t *size);

#ifdef __cplusplus
}
#endif
#endif
