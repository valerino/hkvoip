#ifndef __log_api_h__
#define __log_api_h__
#define LOG_TAG "vvv"
//int process_get_name(pid_t pid, char *process, int process_size);

#define LOGFILE_E(...)
#define LOGFILE_W(...)
#define LOGFILE_D(...)
#define LOGFILE_I(...)

#ifdef NDEBUG
#define LOGE(...)
#define LOGW(...)
#define LOGD(...)
#define LOGI(...)
#else
#include <android/log.h>
#define LOGE(...) LOGFILE_E(__VA_ARGS__) \
    __android_log_print(ANDROID_LOG_ERROR, LOG_TAG, __VA_ARGS__)
#define LOGW(...) LOGFILE_W(__VA_ARGS__) \
    __android_log_print(ANDROID_LOG_WARN, LOG_TAG, __VA_ARGS__)
#define LOGD(...) LOGFILE_D(__VA_ARGS__) \
    __android_log_print(ANDROID_LOG_DEBUG, LOG_TAG, __VA_ARGS__)
#define LOGI(...) LOGFILE_I(__VA_ARGS__) \
    __android_log_print(ANDROID_LOG_INFO, LOG_TAG, __VA_ARGS__)
#endif // NDEBUG
#endif // __log_api_h__