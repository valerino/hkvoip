#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fts.h>
#include <fcntl.h>

int file_get_size_fp(FILE *f, size_t *size) {
    if (!size || !f) {
        return EINVAL;
    }
    *size = 0;
    struct stat st;
    if (fstat(fileno(f), &st) != 0) {
        return errno;
    }
    *size = st.st_size;
    return 0;
}

int file_to_buffer(const char *path, unsigned char **buf, size_t *size) {
    int res = 0;
    unsigned char *mem = NULL;
    if (!buf || !size) {
        return EINVAL;
    }
    *buf = NULL;
    *size = 0;
    
    // get file size
    FILE *f = fopen(path, "rb");
    if (!f) {
        res = errno;
        goto __exit;
    }

    // get size
    size_t s;
    res = file_get_size_fp(f, &s);
    if (res != 0) {
        res = errno;
        goto __exit;
    }
    if (s == 0) {
        res = EOF;
        goto __exit;
    }

    // allocate memory
    mem = (unsigned char *)calloc(1, s + 1);
    if (!mem) {
        res = ENOMEM;
        goto __exit;
    }

    // read file
    res = fread(mem, 1, s, f);
    if (res != s) {
        res = EIO;
        goto __exit;
    }

    // done
    *size = s;
    *buf = mem;
    res = 0;

__exit:
    if (f) {
        // close file anyway
        fclose(f);
    }
    if (res != 0 && mem) {
        // free memory on error
        free(mem);
    }
    return res;
}

int file_get_size(const char *path, size_t *size) {
    if (!size || !path) {
        return EINVAL;
    }

    *size = 0;
    FILE *f = fopen(path, "rb");
    if (!f) {
        return errno;
    }
    int err = file_get_size_fp(f, size);
    fclose(f);
    if (err != 0) {
        return err;
    }
    return 0;
}

int file_recursive_delete(const char *dir) {
    int ret = 0;
    FTS *ftsp = NULL;
    FTSENT *curr;
    char *files[] = {(char *)dir, NULL};

    ftsp = fts_open(files, FTS_NOCHDIR | FTS_PHYSICAL | FTS_XDEV, NULL);
    if (!ftsp) {
        ret = errno;
        goto finish;
    }

    while ((curr = fts_read(ftsp))) {
        switch (curr->fts_info) {
        case FTS_NS:
        case FTS_DNR:
        case FTS_ERR:
            break;

        case FTS_DC:
        case FTS_DOT:
        case FTS_NSOK:
            break;

        case FTS_D:
            break;

        case FTS_DP:
        case FTS_F:
        case FTS_SL:
        case FTS_SLNONE:
        case FTS_DEFAULT:
            if (remove(curr->fts_accpath) < 0) {
                ret = errno;
            }
            break;
        }
    }

finish:
    if (ftsp) {
        fts_close(ftsp);
    }

    return ret;
}
