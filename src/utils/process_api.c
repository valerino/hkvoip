// TODO: Darwin
#include <dirent.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <stdbool.h>
#include "log_api.h"
#include <sys/types.h>
#include <sys/wait.h>

/**
 * @brief execute process using execv
 * @param argv commandline as array of strings
 * @param wait true to wait for completion
 */
int process_exec(const char **argv, bool wait)
{
    if (argv == NULL)
    {
        errno = EINVAL;
        return -1;
    }

    // fork
    int res = -1;
    pid_t pid = fork();
    if (pid == -1)
    {
        return -1;
    }
    if (pid == 0)
    {
        // child
        /*
        int i = 0;
        while (true) {
            DBG_OUT(DBG_DEBUG, "execv(), argv[%d]=%s\n", i, argv[i]);
            i++;
            if (argv[i] == NULL) {
                break;
            }
        }
        */
        // execv do not return
        execv(argv[0], (char **)argv);
    }

    // parent
    if (wait)
    {
        int status = 0;
        res = waitpid(pid, &status, 0);
        if (WIFEXITED(status))
        {
            // get the exitcode
            res = WEXITSTATUS(status);
        }
    }
    else
    {
        // assume success, no wait for child termination
        res = 0;
    }
    return res;
}

int process_find_module(pid_t pid, const char *module, uintptr_t *start,
                        uintptr_t *end, char *path, int path_size)
{
    if (!module)
    {
        return EINVAL;
    }
    int res = ENOENT;

    // read pid maps
    char buffer[1024] = {0};
    sprintf(buffer, "/proc/%d/maps", pid);
    FILE *f = fopen(buffer, "r");
    if (!f)
    {
        res = errno;
        LOGE("can't load maps for pid %d\n", pid);
        return res;
        ;
    }

    // look for bare dll name
    char *barename = strrchr(module, '/');
    if (barename)
    {
        barename += 1;
    }
    else
    {
        barename = (char *)module;
    }
    LOGI("looking for %s ...", barename);

    bool found = false;
    uintptr_t _start;
    uintptr_t _end;
    char _path[260];
    while (fgets(buffer, sizeof(buffer), f) != NULL)
    {
        // check if the line contains our module barename
        if (!strcasestr(buffer, barename))
        {
            // no, go on
            continue;
        }

        // exit on the first entry found
        found = true;
        char str1[20];
        char str2[20];
        char str3[20];
        char str4[20];
        sscanf(buffer, "%p-%p %s %s %s %s %s", (void **)&_start, (void **)&_end,
               str1, str2, str3, str4, _path);
        // LOGD("found %s mapped at %p-%p in pid %d, mapped_path=%s", module,
        // (void*)_start, (void*)_end, pid, _path);
        if (found)
        {
            // return found stuff
            if (start)
            {
                *start = _start;
            }
            if (end)
            {
                *end = _end;
            }
            if (path && path_size)
            {
                strncpy(path, _path, path_size);
            }
            res = 0;
            break;
        }
    }

    // done
    fclose(f);
    return res;
}

bool process_is_64bit(pid_t pid)
{
    bool found = false;
    char module[] = "/bin/linker64";

    // read pid maps
    char buffer[1024] = {0};
    sprintf(buffer, "/proc/%d/maps", pid);
    FILE *f = fopen(buffer, "r");
    if (!f)
    {
        LOGE("can't load maps for pid %d\n", pid);
        return false;
    }

    while (fgets(buffer, sizeof(buffer), f) != NULL)
    {
        // check if the line contains our module path
        if (!strcasestr(buffer, module))
        {
            // no, go on
            continue;
        }
        found = true;
        break;
    }

    // done
    fclose(f);
    return found;
}

int process_get_pid(const char *process_name, pid_t *pids, int32_t pids_size,
                    int *num_pids)
{
    int id;
    pid_t pid = -1;
    DIR *dir;
    FILE *fp;
    struct dirent *entry;

    if (process_name == NULL || pids_size == 0 || num_pids == NULL ||
        pids == NULL)
    {
        return ENOENT;
    }
    *num_pids = 0;
    memset(pids, 0, pids_size * sizeof(pid_t));

    dir = opendir("/proc");
    if (dir == NULL)
    {
        return EACCES;
    }

    int count = 0;
    while ((entry = readdir(dir)) != NULL)
    {
        id = atoi(entry->d_name);
        if (id != 0)
        {
            char filename[64] = {0};
            sprintf(filename, "/proc/%d/cmdline", id);
            fp = fopen(filename, "r");
            if (fp)
            {
                char cmdline[256] = {0};
                fgets(cmdline, sizeof(cmdline), fp);
                fclose(fp);

                if (strlen(cmdline) && (strcmp(process_name, cmdline) == 0))
                {
                    // process found
                    pids[count] = id;
                    count++;
                    if (count == pids_size)
                    {
                        // buffer exhausted
                        break;
                    }
                }
            }
        }
    }
    closedir(dir);

    // finished enumeration
    if (count == 0)
    {
        // pid not found
        return ENOENT;
    }

    // return number of pids
    *num_pids = count;

    return 0;
}

int process_get_name(pid_t pid, char *process, int process_size)
{
    if (process == NULL || process_size == 0)
    {
        errno = EINVAL; // invalid param
        return -1;
    }
    memset(process, 0, sizeof(process_size));

    // read from proc/pid/cmdline
    char s[260];
    pid_t p = pid;
    if (p == 0)
    {
        // 0 = current process
        p = getpid();
    }
    snprintf(s, sizeof(s), "/proc/%d/cmdline", p);

    FILE *f = fopen(s, "rb");
    if (!f)
    {
        // can'/t open process, probably permissions ?!
        return errno;
    }

    char buf[1024] = {0};
    while (!feof(f))
    {
        char c[8] = {0};
        fread(&c, 1, 1, f);
        if (*c == '\0')
        {
            // avoid read further than argv[0]
            break;
        }
        strcat(buf, c);
    }
    if (*buf == '\0')
    {
        // cmdline is empty, error
        errno = ENODATA;
        return errno;
    }

    // get process name only if possible
    char *name = strrchr(buf, '/');
    if (!name)
    {
        name = buf;
    }
    else
    {
        name++;
    }
    strncpy(process, name, process_size - 1);

    return 0;
}
