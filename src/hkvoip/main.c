#include <libutils.h>
#include <frida-gum.h>
#include "hooks.h"
#include "hkvoip.h"
#include <sys/stat.h>
#include <sys/types.h>

bool _run_already_called = false;
GumInterceptor *interceptor = NULL;

/**
 * perform real gum and hooks initialization
 */
void post_init(const char *params)
{

    char tmp[] = "/data/local/tmp";

    // initialize gum
    gum_init_embedded();
    interceptor = gum_interceptor_obtain();

    // initialize lib and parse parameters
    audio_capture_initialize(tmp, (const char *)params);

    // install hooks
    LOGD("run() called, library injected into pid %d", getpid());
    hooks_install(interceptor);

    // release resources
    gum_interceptor_flush(interceptor);
    g_object_unref(interceptor);
    interceptor = NULL;

    // start the audio encoding thread
    start_encoding_thread();
}

/**
 * this is the method called directly by the loader with parameters in global mode
 * note that this is always called ALSO by the constructor, ALWAYS with data=NULL
 */
void __attribute__((visibility("default"))) run(const gchar *data, gboolean *stay_resident)
{
    // configuration file is needed only when in melted mode
    char process_name[260] = {0};
    process_get_name(0, process_name, 260);
    if (_run_already_called)
    {
        // called again, this means we've been called both by the constructor and then by run(): we're in global mode
        LOGW(
            "perform initialization with params=%s",
            data);
        *stay_resident = true;
        post_init(data);
        return;
    }

    // assume global mode without a parameter file
    LOGD("we'll be called again by run(), now just exit");
    _run_already_called = true;
}

/**
 * constructor will run both in melted or global mode.
 * in global mode, run() will be called twice (by the constructor, and when calling run() by the loader) but gum and hooks initialization
 * is granted to happen only once.
 * in melted mode, we must rely on constructor ( we can't pass a parameter,
 * unless adding the complexity of jni. so, we must create a file /storage/emulated/0/Android/data/appname/__vcfg
 * with the parameters string, which will be used by run())
 */
void __attribute__((constructor)) init()
{
    // run
    LOGI("constructor!");
    run(NULL, NULL);
}
