#ifndef __HOOKS_H__
#define __HOOKS_H__
#include <frida-gum.h>

/**
 * @brief install hooks on the voip APIs
 *
 * @param interceptor an instance of frida's GumInterceptor
 * @return 0 on success
 */
int hooks_install(GumInterceptor* interceptor);

#endif


