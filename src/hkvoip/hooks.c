#include <libutils.h>
#include <frida-gum.h>
#include "android_audio_internal.h"
#include "hkvoip.h"

/**
 * @brief defines an entry in the hooks array
 */
typedef struct _HookDefStruct
{
    // the symbol name
    char proto[260];
    // pointer to the hook function
    void *hookFunc;
} HookDefStruct;

/**********************************************************************
 *
 * releaseBuffer hooks
 *
 **********************************************************************/

gpointer ptrAudioRecordReleaseBuffer = NULL;
typedef void (*PTR_AudioRecord_ReleaseBuffer)(void *thisPtr,
                                              Buffer *audioBuffer);
gpointer ptrAudioTrackReleaseBuffer = NULL;
typedef void (*PTR_AudioTrack_ReleaseBuffer)(void *thisPtr,
                                             Buffer *audioBuffer);

/*
 * AudioRecord::releaseBuffer()
 */
void hookAudioRecordReleaseBuffer(void *thisPtr, Buffer *audioBuffer)
{
    // LOGD("hook AudioRecord::releaseBuffer()");

    // log buffer
    audio_collect_data(thisPtr, CAPTURE_TYPE_MIC,
                       (unsigned char *)audioBuffer->raw, audioBuffer->size, 0,
                       0, 0, 0, NULL);

    // call original
    PTR_AudioRecord_ReleaseBuffer p =
        (PTR_AudioRecord_ReleaseBuffer)ptrAudioRecordReleaseBuffer;
    p(thisPtr, audioBuffer);
}

/*
 * AudioTrack::releaseBuffer()
 */
void hookAudioTrackReleaseBuffer(void *thisPtr, Buffer *audioBuffer)
{
    // LOGD("hook AudioTrack::releaseBuffer()");

    audio_collect_data(thisPtr, CAPTURE_TYPE_SPEAKERS,
                       (unsigned char *)audioBuffer->raw, audioBuffer->size, 0,
                       0, 0, 0, NULL);

    // call original
    PTR_AudioTrack_ReleaseBuffer p =
        (PTR_AudioTrack_ReleaseBuffer)ptrAudioTrackReleaseBuffer;
    p(thisPtr, audioBuffer);
}

/**
 * @brief AudioTrack::releaseBuffer symbols for different androids
 */
const HookDefStruct audioTrackReleaseBufferHooksArray[] = {
    {"_ZN7android10AudioTrack13releaseBufferEPNS0_6BufferE",
     (void *)hookAudioTrackReleaseBuffer},

    {"_ZN7android10AudioTrack13releaseBufferEPKNS0_6BufferE",
     (void *)hookAudioTrackReleaseBuffer},

    {"\0", NULL}};

/**
 * @brief AudioRecord::releaseBuffer symbols for different androids
 */
const HookDefStruct audioRecordReleaseBufferHooksArray[] = {
    {"_ZN7android11AudioRecord13releaseBufferEPNS0_6BufferE",
     (void *)hookAudioRecordReleaseBuffer},
    {"_ZN7android11AudioRecord13releaseBufferEPKNS0_6BufferE",
     (void *)hookAudioRecordReleaseBuffer},
    {"\0", NULL}};

/**
 * @brief internal, try to find an export scanning an array of symbol names
 * @return pointer to the resolved symbol, or NULL if not found
 */
void *api_get_ptr(const char *so, HookDefStruct *hooks,
                  HookDefStruct *matching)
{
    HookDefStruct *h = hooks;
    while (h->proto[0] != '\0')
    {
        void *ptr = (gpointer)gum_module_find_export_by_name(so, h->proto);
        if (!ptr)
        {
            LOGW("CAN'T FIND %s in %s", h->proto, so);
        }
        else
        {
            LOGI("FOUND %s in %s !!!", h->proto, so);
            // copy back the entry
            memcpy((char *)matching, (char *)h, sizeof(HookDefStruct));
            return ptr;
        }

        // next
        h++;
    }
    return NULL;
}

int hooks_get_ptrs(const char *so, HookDefStruct *matchingAudioRecord,
                   HookDefStruct *matchingAudioTrack) {
    // hook AudioRecord::releaseBuffer()
    ptrAudioRecordReleaseBuffer =
        api_get_ptr(so, (HookDefStruct *)audioRecordReleaseBufferHooksArray,
                    matchingAudioRecord);
    if (!ptrAudioRecordReleaseBuffer)
    {
        LOGW("AudioRecord::releaseBuffer() not found");
        return ENOENT;
    }
    LOGI("found AudioRecord::releaseBuffer() at %p",
         ptrAudioRecordReleaseBuffer);

    // get AudioTrack pointers
    ptrAudioTrackReleaseBuffer =
        api_get_ptr(so, (HookDefStruct *)audioTrackReleaseBufferHooksArray,
                    matchingAudioTrack);
    if (!ptrAudioTrackReleaseBuffer)
    {
        LOGW("AudioTrack::releaseBuffer() not found");
        return ENOENT;
    }
    LOGI("found AudioTrack::releaseBuffer() at %p", ptrAudioTrackReleaseBuffer);
    return 0;
}

int hooks_install_android(GumInterceptor *interceptor)
{
    HookDefStruct matchingAudioRecord = {0};
    HookDefStruct matchingAudioTrack = {0};
    GumReplaceReturn res = GUM_REPLACE_WRONG_SIGNATURE;

    char process[260] = {0};
    process_get_name(getpid(), process, 260);
    LOGI("current process: %s", process);

    // get pointers to hook, try with libaudioclient.so (android 8+)
    LOGI("*** trying to get pointers in libaudioclient.so ***");
    int r = hooks_get_ptrs("libaudioclient.so", &matchingAudioRecord,
                           &matchingAudioTrack);
    if (r != 0) {
        // last chance, frida will try to look in all loaded modules....
        LOGI("*** trying to get pointers in whatever module found..... (last chance!) ***");
        r = hooks_get_ptrs(NULL, &matchingAudioRecord, &matchingAudioTrack);
        if (r != 0)
        {
            // fail!
            LOGE("Can't find pointers to hook in %s, aborting", process);
            return ENOENT;
        }
    }

    LOGI("hooking AudioRecord::releaseBuffer() replacing %s (%p) with hook %p",
         matchingAudioRecord.proto, ptrAudioRecordReleaseBuffer,
         matchingAudioRecord.hookFunc);
    res = gum_interceptor_replace(interceptor, ptrAudioRecordReleaseBuffer,
                                  (gpointer)matchingAudioRecord.hookFunc, NULL, NULL);
    if (res != GUM_REPLACE_OK) {
        LOGE("Can't hook AudioRecord::releaseBuffer() in %s, res=%d", process, res);
        return res;
    }

    LOGI("hooking AudioTrack::releaseBuffer() replacing %s (%p) with hook %p",
         matchingAudioTrack.proto, ptrAudioTrackReleaseBuffer,
         matchingAudioTrack.hookFunc);
    res = gum_interceptor_replace(interceptor, ptrAudioTrackReleaseBuffer,
                                  (gpointer)matchingAudioTrack.hookFunc, NULL, NULL);

    if (res != GUM_REPLACE_OK) {
        LOGE("can't hook AudioTrack::releaseBuffer()in %s, res=%d", process, res);
        gum_interceptor_revert(interceptor, ptrAudioRecordReleaseBuffer);
        return res;
    }
    // done
    LOGI("AudioTrack and AudioRecord paths correctly hooked in %s !", process);
    return 0;
}

int hooks_install(GumInterceptor* interceptor) {
  if (!interceptor) {
    return EINVAL;
  }
  gum_interceptor_begin_transaction (interceptor);
  int res = hooks_install_android(interceptor);
  gum_interceptor_end_transaction (interceptor);
  return res;
}

