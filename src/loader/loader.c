#include <dlfcn.h>
#include <frida-core.h>
#include <libutils.h>
#include <string.h>
#include <sys/types.h>
#include <sys/xattr.h>
#include <unistd.h>
#include <buildinfo.h>

#define MAX_STRING_SIZE 1024

/**
 * @brief purge unused files in temporary folder (TODO: try to avoid creation,
 * probably there's a frida api about it...)
 */
void purge_frida_tmp()
{
    char tmp[] = "/data/local/tmp";

    // locate any frida folder in tmp
    DIR *dir;
    struct dirent *entry;
    dir = opendir(tmp);
    if (dir == NULL)
    {
        return;
    }

    int count = 0;
    while ((entry = readdir(dir)) != NULL)
    {
        if (strstr(entry->d_name, "frida"))
        {
            // found a frida folder
            char full_path[260];
            sprintf(full_path, "%s/%s", tmp, entry->d_name);
            LOGI("purging frida folder: %s", full_path);
            file_recursive_delete(full_path);
        }
    }
    closedir(dir);
}

/*
 * @brief inject into the given process (internal)
 *
 * @param s process name to inject into
 * @param argc from main
 * @param argv from main
 * argv layout: argv[0] <process|process_csv>
 * </path/to/64bit_so[,/path/to/32bit_so]>
 * [q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10]);
 * @return 0 on success
 */
int inject_internal(char *s, int argc, char **argv)
{
    pid_t pids[128] = {0};
    int num_pids = 0;
    int res = process_get_pid(s, pids, 128, &num_pids);
    if (res != 0)
    {
        return res;
    }

    char to_inject_path_64[MAX_STRING_SIZE] = {0};
    strncpy(to_inject_path_64, argv[2], MAX_STRING_SIZE);
    char *to_inject = to_inject_path_64;

    // there may be more than a process in argv[1]
    for (int i = 0; i < num_pids; i++)
    {
        to_inject = to_inject_path_64;

        // check if it's already mapped first...
        if (process_find_module(pids[i], to_inject, NULL, NULL, NULL, 0) == 0)
        {
            // process is already mapped
            LOGW(
                "%s already mapped in process %s, pid=%d, skipping injection",
                to_inject, s, pids[i]);
            continue;
        }

        // check if the file exists first, or frida will crash the application
        // during injection!
        if (access(to_inject, F_OK) == -1)
        {
            LOGE("error, can't find library to be injected: %s", to_inject);
            continue;
        }

        // we need to set selinux context on the file to inject, first (or i.e. samsung android10 crashes!)
        const char *context = "u:object_r:frida_file:s0";
        int set_context_res = setxattr(to_inject, XATTR_NAME_SELINUX, context, strlen(context) + 1, 0);
        if (set_context_res != 0)
        {
            LOGE("error setting selinux context on %s", to_inject);
            continue;
        }

        // ok, inject!
        GError *err = NULL;
        LOGI("injecting %s into process %s, pid=%d, 64bit=%d, argc=%d, argv[3]=%s", to_inject, s, pids[i], TRUE, argc, argc >= 4 ? argv[3] : "dummy");
        FridaInjector *injector = frida_injector_new();
        guint id = frida_injector_inject_library_file_sync(injector, pids[i], to_inject, "run", argc >= 4 ? argv[3] : "dummy", NULL, &err);
        if (err == NULL)
        {
            // injection done
            frida_injector_close_sync(injector, NULL, NULL);
        }
        else
        {
            LOGE("error injection into process %s, pid=%d: %s", s, pids[i], err->message);
            g_error_free(err);
        }
        g_object_unref(injector);

        // purge frida temp dir, it's not needed
        purge_frida_tmp();
    }

    return 0;
}

/*
 * @brief inject into the given processes at argv[1]
 *
 * @param argc from main
 * @param argv from main
 * argv layout: argv[0] <process|process_csv>
 * </path/to/64bit_so[,/path/to/32bit_so]> <receiver>
 * [q(uality)=0-10,v(br)=0|1,c(omplexity)=1-10]);
 */
void inject(int argc, char **argv)
{
    // get the process csv
    char processes[MAX_STRING_SIZE] = {0};
    strncpy(processes, argv[1], MAX_STRING_SIZE);
    char *s = strtok(processes, ",");
    if (s == NULL)
    {
        // single process
        strcat(processes, ",dummy");
        s = strtok(processes, ",");
    }

    // loop injecting into processes
    while (s != NULL)
    {
        // inject into this process
        inject_internal(s, argc, argv);
        // next
        s = strtok(NULL, ",");
    }
}

int main(int argc, char **argv)
{
    LOGD("loader started, version %s-%d-%s", GIT_COMMIT, GIT_BUILDNUMBER, GIT_BRANCH);

    if (argc < 3)
    {
#ifndef NDEBUG
        printf(
            "usage: %s <process|process_csv> "
            "</absolute/path/to/64bit_so> "
            "[b=video_bitrate_default_500000,"
            "m=video_delay_offset_sec_default_1.0,"
            "a=mic_audio_delay_offset_sec_default_0.5,"
            "p=/absolute/path/to/destination_for_audio_files_default_data/local/tmp,"
            "r=android_broadcast_receiver_name_to_receive_video_file_path_when_ready_default_none,"
            "v=video_resolution_default_800x600]",
            argv[0]);
#endif
        return 1;
    }

    // init frida
    LOGD("initializing frida");
    frida_init();

    // patch selinux
    LOGD("patching selinux");
    frida_selinux_patch_policy();

    LOGD("starting injection loop");
    while (1)
    {
        // loop
        inject(argc, argv);

        // wait 10 seconds
        sleep(10);
    }

    // never reached directly ....
    frida_deinit();
    return 0;
}
