supolicy --live \
"allow * frida_file fifo_file { execute open create write read unlink  }" \
"allow * sec_debugfs file { read write execute open create unlink }" \
"allow * shell_data_file dir { open remove_name read write add_name create search }" \
"allow * shell_data_file file { execute open create write read unlink execute_no_trans }" \
"allow * unlabeled dir { open remove_name read write add_name create search }" \
"allow * unlabeled fifo_file { execute open create write read unlink }" \
"allow * unlabeled fifo_file { write }" \
"allow frida_file labeledfs filesystem { associate }" \
"allow init init process { ptrace }" \
"allow init isolated_app process { signal }" \
"allow init kernel security { read_policy load_policy setenforce }" \
"allow init media_rw_data_file file { execute execute_no_trans }" \
"allow init platform_app process { ptrace }" \
"allow init s_isolated_app process { signal }" \
"allow init s_platform_app process { ptrace }" \
"allow init s_untrusted_app process { ptrace signal sigstop }" \
"allow init untrusted_app process { ptrace signal sigstop }" \
"allow isolated_app shell_data_file file { execute open create write read unlink }" \
"allow magisk magisk_file sock_file { ioctl }" \
"allow mediaserver shell_data_file dir { open remove_name read write add_name create }" \
"allow mediaserver shell_data_file dir { open remove_name read write add_name create }" \
"allow mediaserver shell_data_file file { open read write create getattr execute execute_no_trans }" \
"allow mediaserver shell_data_file file { open read write create getattr execute execute_no_trans }" \
"allow s_untrusted_app app_data_file file { execute execute_no_trans open create read write unlink}" \
"allow s_untrusted_app media_rw_data_file file { execute open create read write unlink execute_no_trans }" \
"allow s_untrusted_app shell_data_file dir { open remove_name read write add_name create setattr }" \
"allow s_untrusted_app shell_data_file file { execute open create search read write unlink execute_no_trans setattr }" \
"allow s_untrusted_app system_file file { execmod open create read write unlink execute execute_no_trans }" \
"allow s_untrusted_app zygote_tmpfs file { execute execute_no_trans }" "allow shell default_prop property_service { set }" \
"allow servicemanager zygote dir { search }" \
"allow servicemanager zygote file { read open }" \
"allow servicemanager zygote process { getattr }" \
"allow system_app shell_data_file dir { open remove_name read write add_name create }" \
"allow system_app shell_data_file file { open read write create getattr execute execute_no_trans }" \
"allow system_server shell_data_file dir { open remove_name read write add_name create }" \
"allow system_server shell_data_file file { open read write create getattr execute execute_no_trans }" \
"allow system_server zygote binder { call }" \
"allow untrusted_app app_data_file file { open execute execute_no_trans create write read unlink }" \
"allow untrusted_app media_rw_data_file file { execute execute_no_trans }" \
"allow untrusted_app shell_data_file dir { open remove_name read write add_name create setattr }" \
"allow untrusted_app shell_data_file file { execute open create write read unlink execute_no_trans setattr }" \
"allow untrusted_app system_file file { execmod open create read write unlink execute execute_no_trans }" \
"allow untrusted_app zygote_tmpfs file { execute execute_no_trans }" \
"allow untrusted_app_27 * fifo_file { open read write create getattr }" \
"allow untrusted_app_27 frida_file dir { open remove_name read write add_name create search }" \
"allow untrusted_app_27 proc file { open create read write }" \
"allow untrusted_app_27 rootfs dir { open read write create search }" \
"allow untrusted_app_27 shell_data_file fifo_file { execute open create write read unlink  }" \
"allow untrusted_app_27 unlabeled fifo_file { open create read write }" \
"allow zygote activity_service service_manager { find }" \
"allow zygote servicemanager binder { call }" \
"allow zygote system_server binder { call transfer }"
