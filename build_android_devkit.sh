#!/usr/bin/env bash
if [ "$1" == "" ]; then
	echo "usage: $0 <frida_repo_root> <--clean to rebuild, resets frida repo to HEAD and delete existing built sdk>"
	exit 1
fi

if [ ! -d "$1" ]; then
	echo "$1 must exists and contain frida repo!"
	exit 1
fi

if [ "$ANDROID_NDK_ROOT" == "" ]; then
	echo "ANDROID_NDK_ROOT must be set!"
	exit 1
fi

__CWD=$(pwd)
if [ "$2" == "--clean" ]; then
	echo ". cleaning devkit"
	rm -rf ./devkit-android-arm64
	echo ". cleaning frida repo"
	cd $1
	if [ $? -eq 0 ]; then
		make clean
		git reset --hard --recurse-submodules
	fi
	cd $__CWD
fi

echo ". fixing tmp folder getuid() check for frida-core"
sed 's/if (getuid/\/\/ if getuid/g' "$1/frida-core/src/linux/system-linux.c" >./tmpl
cp ./tmpl "$1/frida-core/src/linux/system-linux.c"
rm ./tmpl

echo ". building frida core"
cd $1
make core-android-arm64
if [ ! $? -eq 0 ]; then
	exit 1
fi

echo ". assembling frida libs (arm64)"
mkdir -p $__CWD/devkit-android-arm64/lib
mkdir -p $__CWD/devkit-android-arm64/include
./releng/devkit.py frida-core android-arm64 ./build/frida-android-arm64
./releng/devkit.py frida-gum android-arm64 ./build/frida-android-arm64
cp ./build/frida-android-arm64/libfrida-core.a $__CWD/devkit-android-arm64/lib/
cp ./build/frida-android-arm64/libfrida-gum.a $__CWD/devkit-android-arm64/lib/
cp ./build/frida-android-arm64/frida-core.h $__CWD/devkit-android-arm64/include
cp ./build/frida-android-arm64/frida-gum.h $__CWD/devkit-android-arm64/include
cd $__CWD
