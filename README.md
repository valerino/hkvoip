# VOIP hooking framework (revisited)

> for "historic" version with chunking and no video, look in *last_with_working_chunks_no_video* tag.

* updated to android 12 (probably 13 works too, didnt tried)
* capture audio via AudioRecord/AudioTrack hooking and video via *screencapture* (needs patched version to support more than 180sec), rebuilds mp4 via *ffmpeg*.
  * *ffmpeg* and *screencapture* binaries are not included, this is meant to be used with [jailrec](https://bitbucket.org/valerino/jailrec)

## build

~~~bash
export ANDROID_NDK_ROOT=/path/to/android-ndk

# download and build frida (android arm64 only), if not already built
git clone --recurse-submodules https://github.com/frida/frida
./build_android_devkit.sh ./frida

# build (clean first, build debug version)
/build.sh --clean && ./build.sh --debug  
~~~

## patch selinux

[selinux patches script](./patch_selinux.sh) is included, to be run on rooted device.