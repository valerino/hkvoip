#!/usr/bin/env bash
# generates buildinfo.h with git commit hash
if [ -z "$1" ]; then
    echo "usage: $0 </path/to/buildinfo.h>"
    exit 1
fi

# get info
_GIT_COMMIT=$(git rev-parse --short HEAD)
_GIT_BRANCH=$(git rev-parse --abbrev-ref HEAD)
_GIT_BUILDNUMBER=$(git log --pretty=oneline | wc -l)
rm "$1"
echo "#pragma once" >"$1"
echo "#define GIT_COMMIT \"$_GIT_COMMIT\"" >>"$1"
echo "#define GIT_BRANCH \"$_GIT_BRANCH\"" >>"$1"
_GIT_BUILDNUMBER="${_GIT_BUILDNUMBER#"${_GIT_BUILDNUMBER%%[![:space:]]*}"}"
echo "#define GIT_BUILDNUMBER $_GIT_BUILDNUMBER" >>"$1"

echo "[.] branch=$_GIT_BRANCH, commit-id=$_GIT_COMMIT, buildnumber=$_GIT_BUILDNUMBER"
echo "[.] done, generated $1 !"
