#!/usr/bin/env bash
# build native binaries
__BUILD_DIR="./libs-debug"
__FLAVOR="debug"

#if [ $# -lt 1 ]; then
#    echo 'usage:' $0 '<--debug|--release|--clean>'
#    exit 1
#fi

if [ "$1" == "--release" ]; then
    __BUILD_DIR="./libs-release"
    __FLAVOR="release"
    echo '[.] building release version to' "$__BUILD_DIR"
elif [ "$1" == "--debug" ]; then
    echo '[.] building debug version to' "$__BUILD_DIR"
elif [ "$1" == "--clean" ]; then
    echo '[.] cleanup build folders'
    rm -rf ./libs-debug
    rm -rf ./libs-release
    rm -rf ./obj
    exit 0
else
    echo 'usage:' "$0" '<--debug|--release|--clean> [--push to push built binaries to devices calling ./copy_to_device.sh]'
    exit 1
fi

# build
./generate_buildinfo_h.sh ./src/buildinfo.h
if [ "$1" == "--release" ]; then
    "$ANDROID_NDK_ROOT/ndk-build" -C ./jni NDK_APP_LIBS_OUT=".$__BUILD_DIR"
else
    "$ANDROID_NDK_ROOT/ndk-build" -C ./jni NDK_DEBUG=1 NDK_APP_LIBS_OUT=".$__BUILD_DIR"
fi

if [ "$?" -ne 0 ]; then
    rm -rf "$__BUILD_DIR"
    exit 1
fi

echo '[.] done, built to' "$__BUILD_DIR"

if [ "$2" == '--push' ]; then
    # push to device
    ./copy_to_device.sh "$1"
fi
